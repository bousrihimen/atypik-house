@extends ('base.base')
@section('content')
    <div class="titre">
        <h4>Nous Contacter</h4>
    </div>
    <div class="contact">



        <ul class="list">

            <li class="text-green-500">E-mail<i class="fa-solid fa-envelope"></i></li>

            <a href="/nouscontacter"><li>Téléphone <i class="fa-solid fa-phone"></i></li></a>

        </ul>

        <hr>



    </div>

    <div class="form-contact">
        <form action="" id="formulaire" class="form">
            @csrf


            <div class="div">
                <div>
                    <label for="nom">Nom <span class="oblig label">*</span></label>
                </div>
                <input type="text" class="form-control" id="name" placeholder="Dupont">
                <input type="text" class="form-control" id="prenom" placeholder="Jean"><br>
            </div>


            <div class="div">
                <div>
                    <label for="sujet">Sujet <span class="oblig label">*</span></label>
                </div>
                <select class="form-control" name="sujet" id="sujet">
                    <option option value="">-Selectionnez une question... </option>
                    <option value="technique">Problème Technique</option>
                    <option value="reservation">Reservation</option>
                    <option value="payment">Payement</option>
                    <option value="annulation">Annulation</option>
                    <option value="modif">Modification</option>
                    <option value="autre">Autre</option>
                </select><br>
            </div>
            <div class="div">
                <div>
                    <label for="email">Email de contact <span class="oblig label">*</span></label>
                </div>
                <input type="email" class="form-control" id="email" placeholder="dupont.jean@gmail.com">
            </div>

            <div class="div">
                <textarea name="prob" class="textarea" placeholder="Expliquez-nous votre problème..." id="prob" cols="150" rows="5"></textarea>
            </div>
            <div class="div">
                <input id="btn" name="submit" type="submit" value="Envoyer">

            </div>
        </form>
    </div>
@endsection
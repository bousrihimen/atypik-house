@extends('base.base')
@section('content')

<section class="flex justify-evenly w-full p-4 flex-wrap">

    @foreach($categories as $categorie)
    <a href="/categories/{{$categorie->id}}" class="h-96 w-2/5 mb-8  bg-center bg-cover border border-neutral-500" style="background-image:url({{$categorie->image}})"><h3 class="text-center my-40 text-white text-6xl">{{ $categorie->nom }}</h3></a> 
    @endforeach
</section>

@endsection
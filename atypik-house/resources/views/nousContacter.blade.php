@extends ('base.base')
@section('content')
<section>

<div class="titre"><h4>Nous Contacter</h4></div>
    <div class="contact">

        <ul class="list">

           <a href="/contact"> <li>E-mail <i  class="fa-solid fa-envelope"></i></li></a>
            
            <li class="text-green-500">Téléphone<i class="fa-solid fa-phone"></i></li>

        </ul>

    </div>

        <hr>

        <div id="container">
            <p>09 67 89 75 35</p>
            <p>(APPEL NON SURTAXE)</p>
            <p>DU LUNDI AU VENDREDI DE 9H A 18H</p>
        </div>
    </section>
    
@endsection

@extends('base.base')
@section('content')
        <section class="flex flex-col items-center px-8">
            
            <div class="flex justify-center mb-4">

                <div class="text-center text-gray-500 font-bold text-2xl">
                    Vous êtes propriétaire d'un ou plusieurs <span class="inline-block text-teal-700 font-bold">HEBERGEMENTS INSOLITES</span><br>
                    et vous souhaitez apparaitre sur notre site ? Rien de plus simple
                </div>

            </div>

            <div class="h-1 w-96 bg-green-800 rounded-full mb-6"></div>

            <div class="flex text-gray-500 font-bold justify-center text-lg">
                Voici les 4 étapes à suivre
            </div>

            <div class="flex space-x-5 m-8 justify-evenly mb-8">

                <div class="relative text-xs text-slate-400 text-center py-16 rounded-3xl box-border h-15 w-32 box-content h-180 w-180 p-4 border-2 shadow-md">
                    <span class="absolute bottom-28 left-3 text-green-800 font-bold text-7xl">
                        1
                    </span>
                    Remplir notre formulaire en ligne.
                </div>

                <div class="relative text-xs text-slate-400 text-center pt-12 rounded-3xl box-border h-26 w-32 box-content h-180 w-180 p-4 border-2 shadow-md">
                    <span class="absolute bottom-28 left-3 text-green-800 font-bold text-7xl">
                        2
                    </span>
                    Notre équipe prends connaissance de votre demande et l'étudie.
                    Cela peut prendre quelques jours.
                </div>

                <div class="relative text-xs text-slate-400 text-center pt-12 rounded-3xl box-border h-26 w-32 box-content h-180 w-180 p-4 border-2 shadow-md">
                    <span class="absolute bottom-28 left-3 text-green-800 font-bold text-7xl">
                        3
                    </span>
                    Nous organisons avec vous un rendez-vous téléphonique afin de répondre à vos questions et compléter votre dossier.
                </div>

                <div class="relative text-xs text-slate-400 text-center pt-12 rounded-3xl box-border h-26 w-32 box-content h-180 w-180 p-4 border-2 shadow-md">
                    <span class="absolute bottom-28 left-3 text-green-800 font-bold text-7xl">
                        4
                    </span>
                    Votre hébergement est mis en ligne sous vos conditions et vous commencez à recevoir vos premiers voyageurs Atipik !
                </div>

            </div>

            <div class="relative flex justify-center rounded-3xl box-border h-96 w-96 box-content h-2/3 w-96 p-4 border-2 shadow-md px-36 m-0">

                <form class="mb-2 mt-18 justify-center" action="#" method="post">
                    @csrf
                    <div class="flex flex-col top-3 left-5 mb-2">
                        <h2 class="block mb-1">FORMULAIRE POUR DEVENIR UN HÔTE</h2>
                        <span class="h-2 w-10 bg-green-800 rounded-full mb-4"></span>
                    </div>

                    <div class="flex top-12 left-10 mb-3">
                        <div class="align-self-center h-2 w-2 bg-green-800"></div>
                        <div class="justify-center">Vos coodonnées</div>
                    </div>
                    
                    <div class="flex space-x-20 mb-4"> 
                        <div class="flex flex-col">
                            <label for="nom">Nom</label>
                            <input class="rounded-md border-stone-400 border-2" type="text" name="nom" id="nom" placeholder="Votre Nom">
                                        
                            <label for="email">Email</label>
                            <input class="rounded-md border-stone-400 border-2" type="text" name="email" id="email" placeholder="Votre adresse mail">
                        </div>

                        <div class="flex flex-col">
                            <label for="prenom">Prénom</label>
                            <input class="rounded-md border-stone-400 border-2" type="text" name="prenom" id="prenom" placeholder="Votre Prénom">

                            <label for="num">Numéro de téléphone</label>
                            <input class="rounded-md border-stone-400 border-2" type="text" name="num" id="nom" placeholder="Votre numero de téléphone">
                        </div>
                    </div>

                    <div class="h-2 w-100 bg-green-800 rounded-full mb-4"></div>
                    
                    <div class="flex flex-row mb-2">
                        <div class="align-self-center h-2 w-2 bg-green-800"></div>
                        <div>Type d'hébergements</div>
                    </div>

                    <div class="flex flex-row space-x-10 mb-4">
                            <div class="flex flex-col">
                                <div>
                                    <input type="radio" name="type_logement" id="cabane_arbre">
                                    <label for="cabane_arbre">Cabane dans les arbres</label>
                                </div>
                                <div>
                                    <input type="radio" name="type_logement" id="maison_piloti">
                                    <label for="maison_piloti">Maison sur pilotis</label>
                                </div>
                                <div>
                                    <input type="radio" name="type_logement" id="maison_hobbits">
                                    <label for="maison_hobbits">Maison de Hobbits</label>
                                </div>
                            </div>

                            <div class="flex flex-col">
                                <div>
                                    <input type="radio" name="type_logement" id="bulle">
                                    <label for="bulle">Bulle</label>
                                </div>
                                <div>
                                    <input type="radio" name="type_logement" id="roulotte">
                                    <label for="roulotte">Roulotte</label>
                                </div>
                                <div>
                                    <input type="radio" name="type_logement" id="chalet">
                                    <label for="chalet">Chalet</label>
                                </div>
                            </div>
                                    
                            <div class="flex flex-col">
                                <div>
                                    <input type="radio" name="type_logement" id="yourte">
                                    <label for="yourte">Yourte</label>
                                </div>
                                <div>
                                    <input type="radio" name="type_logement" id="tipi">
                                    <label for="tipi">Tipi</label>
                                </div>
                                <div>
                                    <input type="radio" name="type_logement" id="autre">
                                    <label for="autre">Autre</label>
                                </div>
                            </div>
                        </div>

                        <div class="flex flex-row space-x-20">
                            <div class="flex flex-col">
                            <div class="h-2 w-2 bg-green-800"></div>
                            <label for="nb_hebergement">Nombre d'hébergement(s)</label>
                            <input type="range" min="0" max="10" value="0" id="nb_hebergement" class="slider slider_nb_h mt-2">
                            <p class="text-xs text-slate-500 mt-2">selectionné : <span id="select1"></span></p>
                        </div>

                            <div class="flex flex-col mb-4">
                                <div class="h-2 w-2 bg-green-800"></div>
                                <label for="cap_hebergement">Capacité d'hébergement</label>
                                <input type="range" min="0" max="10" value="0" id="cap_hebergement" class="slider slider_cap_h mt-2">
                                <p class="text-xs text-slate-500 mt-2">selectionné : <span id="select2"></span></p>
                            </div>
                    </div>
                    
                    <label for="infos_hebergement">Informations hébergement :</label><br>
                    <textarea class="flex rounded-md border-stone-400 border-2 justify-center " type="text" name="infos_hebergement" id="infos_hebergement" rows="5" cols="58" placeholder=" (Lieu/Département/Région/Pays) "></textarea><br>
                    
                    <label for="particularites">Ses particularités :</label><br>
                    <textarea class="rounded-md border-stone-400 border-2" type="text" name="particularites" id="particularites" rows="5" cols="58" placeholder=" (Particularités) "></textarea>
                    
                    <div class="flex flex-row mb-2">
                        <div class="h-2 w-2 bg-green-800"></div>
                        <div>Visuel hébergement</div>
                    </div>

                    <div class="flex flex-row justify-around border-neutral-900 ">
                        <button class="self-center h-8 px-2 btn border-2 rounded-md bg-slate-200" id="btn_upload">Choisir un fichier</button>
                        <!-- <button class="self-center px-8 btn border-2 rounded-full bg-green-800 text-white min-w-min h-12" type="submit" id="btn_submit">valider votre demande</button> -->
                        <a href="/devenir-hote-valide"><button class="self-center px-8 btn border-2 rounded-full bg-green-800 text-white min-w-min h-12" id="btn_submit" type="button">valider votre demande</button></a>
                    </div>

                </form> 

            </div>

            <script>
                var slider_nb_h = document.getElementById("nb_hebergement");
                var output_nb_h = document.getElementById("select1");
                output_nb_h.innerHTML = slider_nb_h.value;

                slider_nb_h.oninput = function() {
                    output_nb_h.innerHTML = this.value;
                }

                var slider_cap_h = document.getElementById("cap_hebergement");
                var output_cap_h = document.getElementById("select2");
                output_cap_h.innerHTML = slider_cap_h.value;

                slider_cap_h.oninput = function() {
                    output_cap_h.innerHTML = this.value;
                }
            </script>
     
        </section>
@endsection
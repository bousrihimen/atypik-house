<section class="flex w-11/12 justify-center y-auto border-solid border-pink-400 border-2">

    <aside class="w-2/3 border-solid border-yellow-600 border-2">
        <h1 class="text-light-green-atypik text-4xl mb-3 font-semibold flex justify-center tracking-wide">Voulez-vous personnaliser votre séjour ?</h1>
        
        <div class="flex flex-col space-y-4 border-solid border-red-700 border-2">
            <span class='text-dark-grey-atypik flex justify-center text-xl font-thin'>
                Vos services sont annulables et remboursables jusqu'à 4 jours avant l'arrivée.
            </span>

            <div id="conciergerie" class="flex mx-auto w-4/5 p-3 rounded-lg border-solid border-2 border-blue-600 shadow-xl mb-8">
                <i class="flex2 inline-block fa-solid fa-bell-concierge fa-lg text-dark-grey-atypik"></i>
                <div class="text-dark-grey-atypik font-bold text-sm mx-auto">
                    Service de consergerie (5)
                </span>
                <i class="fa-solid fa-angle-right rotate" id="fleche_conciergerie"></i>
            <div id="conciergerie" class=" hidden h-96 w-full bg-red-400">
                
            </div>
            </div>

                </div>
            </div>
        </div>
        </div>

        <div class="flex my-8 mx-auto w-4/5 p-20 rounded-lg border-solid border-2 border-green-600 shadow-xl">
            <span class="text-gray-400 font-bold text-sm mx-auto">
                Restauration (2)
            </span>
            <i class="fa-solid fa-angle-right rotate" id="fleche_restauration"></i>

            <div>


            </div>

        </div>

    </aside>
    <!-- <aside class=" bg-red-300 w-1/3 h-96"> coté droit -->
    {{-- detaille et contenu --}}

    <div class="mr-8 w-4/12 ml-6 rounded-3xl shadow-gray-600 shadow border-2">
       
        <img src="../img/chalet.jpg" alt="chalet" class="w-full h-[300px] rounded-2xl">
        
        <h3 class="  text-dark-grey-atypik text-2xl ml-8 mt-6 font-semibold"> Chalet des Grues Hautes </h3>
        <h3 class="  text-dark-grey-atypik text-2xl ml-8 mt-2 font-semibold "> Savoyardes
            <span class="fa fa-star border-1 border-light-green-atypik text-light-green-atypik"></span>
            <span class="fa fa-star  text-light-green-atypik"></span>
            <span class="fa fa-star  text-light-green-atypik"></span>
            <span class="fa fa-star  text-light-green-atypik"></span>
            <span class="fa fa-star  text-light-green-atypik"></span>
        </h3>
        <h4 class="mt-4 ml-3 text-xs font-bold text-dark-grey-atypik"> Hébergements</h4>
        <p class=" ml-4 mt-2 text-xs font-bold text-dark-grey-atypik "><i class=' mr-2 h-4 w-4   text-light-green-atypik fas'>&#xf186;</i> 13 nuits de 17/11/2022 au 30/11/2022</p>
        <p class=" ml-4  mt-2 text-xs font-bold text-dark-grey-atypik"><i class=" mr-2 h-4 w-4   text-light-green-atypik fa">&#xf236;</i> 5 personnes - 5 chambres</p>
        <div class=" mt-4 border-1 bg-dark-grey-atypik h-1 mb-6 ml-5 mr-5">
        </div>


        <h3 class=" ml-4 font-bold text-dark-grey-atypik">
            Pour un tarif total du :
        </h3>
        <p class=" mr-4 text-right font-extrabold text-light-green-atypik">806 <i class="text-base text-light-green-atypik fa">&#xf153;</i>
            HT</p>
        <p class="mr-4 mb-20 text-xs text-right font-bold text-gray-400">non flexible <br> non modifiable non remboursable</p>

        <button class="bg-light-green-atypik  w-11/12 ml-5 mb-5 rounded-full  text-light-grey1-atypik  p-2 "><a href="#/../Panier4">
                <h3 class="leading-3 text-lg font-semibold mb-0"> Continuer</h3>
                <h4 class=" text-xs">Sans Personnalisation</h4>
            </a></button>
        <h3 class=" ml-4 font-bold text-gray-400">
            A propos de nos tarifs
        </h3>
        <p class="ml-4 mb-4 text-xs font-semibold text-gray-400 ">Les prix affichés comprennent l'hébergement ainsi que toutes les charge et taxes (excepté la taxe de séjour).
        </p>
        <p class=" ml-4 mb-6 text-xs font-semibold text-gray-400 ">Appelez nos conseillers au 070246100 (0,16<i class="text-sx  fa">&#xf153;</i> /min), du lundi au vendredi de 10h à 19h, le samedi 10h à 17h.</p>
    </div>

    <!-- </aside> -->

</section>
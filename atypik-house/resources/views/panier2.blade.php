@extends('base.base')
@section('content')

<section class="flex mx-auto h-24 m-6 px-8 py-2 p-4 w-3/4 gap-2">

    <div class="place-self-center flex flex-col w-40">
        <i class="flex1 inline-block fa-solid fa-suitcase fa-2xl text-dark-green-atypik"></i>
        <span class="flex justify-center text-xs text-dark-green-atypik">Récapitulatif</span>
    </div>
    
    <div class="inline-block self-center mt-5 w-1/4 mx-auto">
        <div class="w-full bg-dark-green-atypik h-1 mb-6">
            <div class=" bg-dark-green-atypik h-1" style="width: 50%"></div>

        </div>
    </div>

    <div class="place-self-center flex flex-col w-40">
        <i class="flex2 inline-block fa-solid fa-bell-concierge fa-2xl text-dark-green-atypik"></i>
        <span class="flex justify-center text-xs text-dark-green-atypik">Activités & services</span>
    </div>

    <div class="inline-block self-center mt-5 w-1/4 mx-auto">
        <div class="w-full bg-light-grey3-atypik  h-1 mb-6">
            <div class=" bg-dark-green-atypik h-1" style="width: 50%"></div>
        </div>
    </div>

    <div class="place-self-center flex flex-col w-40">
        <i class="flex3 inline-block fa-solid fa-address-card fa-2xl text-light-grey3-atypik"></i>
        <span class="flex justify-center text-xs text-light-grey3-atypik">Coordonnées</span>
    </div>

    <div class="inline-block self-center mt-5 w-1/4 mx-auto">
        <div class="w-full bg-light-grey3-atypik h-1 mb-6">
            <div class=" bg-light-grey3-atypik h-1" style="width: 50%"></div>
        </div>
    </div>

    <div class="place-self-center flex flex-col w-40">
        <i class="flex4 inline-block fa-solid fa-credit-card fa-2xl text-light-grey3-atypik"></i>
        <span class="flex justify-center text-xs text-light-grey3-atypik">Paiement</span>
    </div>
</section>

<section class="flex w-full justify-center y-auto">

    <aside class="flex flex-col w-11/12 justify-start m-4">
        <h1 class="text-light-green-atypik text-4xl mb-3 font-semibold flex justify-center tracking-wide">
            Voulez-vous personnaliser votre séjour ?
        </h1>
        <span class='text-dark-grey-atypik flex justify-center text-xl font-light'>
            Vos services sont annulables et remboursables jusqu'à 4 jours avant l'arrivée.
        </span>

        <div class="flex flex-col mt-8 w-full">
           
            <div id="conciergerie" class="flex justify-between items-center my-8 mx-auto w-4/5 p-8 rounded-lg border-solid border-2 border-light-grey3-atypik shadow-xl mb-8">
                <span class="text-dark-grey-atypik font-bold text-2xl">
                    <i class="fa-solid fa-bell-concierge fa-lg text-dark-grey-atypik"></i>
                    Service de consergerie <span class="text-light-grey3-atypik">(5)</span>
                </span>
                    <i class="fa-solid fa-angle-right rotate" id="fleche_conciergerie"></i>
            </div>

            <div class="flex flex-col mx-auto my-8 w-4/5 h-96 rounded-lg border-solid border-2 border-light-grey3-atypik shadow-xl">
                
                <div class="flex justify-between mx-8 py-4 w-11/12">
                    <span class="text-dark-grey-atypik font-bold text-2xl justify-between">
                    <i class="fa-solid fa-utensils fa-lg text-dark-grey-atypik"></i>
                    Restauration <span class="text-light-grey3-atypik">(2)</span>
                    </span>
                    <i class="fa-solid fa-angle-down rotate" id="fleche_restauration"></i>
                </div>
                <div class="flex justify-center space-x-8">
                    <div class="flex flex-col mt-6 px-6 py-3 border-light-grey3-atypik border-2 shadow-md rounded text-xs text-dark-grey-atypik">
                        <img src="../img/repas.webp" alt="chalet" class="w-full h-[150px] rounded-2xl mb-4">
                        <span class="text-sm font-medium">2 repas de famille (plat + entrée ou dessert)</span>
                        <div class="flex justify-between"><span class="font-thin italic">A partir de 3 ans</span><a class="underline text-light-green-atypik" href="#">Voir plus +</a></div>
                        <div class="flex justify-between place-items-center mt-4"><span class="font-medium">+ 20 € par personne</span>
                        <i class="fa-solid fa-minus"></i><input type="number" class="border-light-grey3-atypik border-2 w-1/5"><i class="fa-solid fa-circle-plus"></i></div>
                    </div>
                    <div class="flex flex-col mt-6 px-6 py-3 border-light-grey3-atypik border-2 shadow-md rounded text-xs text-dark-grey-atypik">
                        <img src="../img/repas.webp" alt="chalet" class="w-full h-[150px] rounded-2xl mb-4">
                        <span class="text-sm font-medium">3 repas de famille (plat + entrée ou dessert)</span>
                        <div class="flex justify-between"><span class="font-thin italic">A partir de 3 ans</span><a class="underline text-light-green-atypik" href="#">Voir plus +</a></div>
                        <div class="flex justify-between mt-4"><span class="font-medium">+ 30 € par personne</span>
                        <i class="fa-solid fa-minus"></i><input type="number" class="border-light-grey3-atypik border-2 w-1/5"><i class="fa-solid fa-circle-plus"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </aside>
    <!-- <aside class=" bg-red-300 w-1/3 h-96"> coté droit -->
    {{-- detaille et contenu --}}

    <div class="mr-8 w-4/12 ml-6 mb-12 rounded-3xl shadow-gray-600 shadow border-2">
       
        <img src="../img/chalet.jpg" alt="chalet" class="w-full h-[300px] rounded-2xl">
        
        <h3 class="  text-dark-grey-atypik text-2xl ml-8 mt-6 font-semibold"> Chalet des Grues Hautes </h3>
        <h3 class="  text-dark-grey-atypik text-2xl ml-8 mt-2 font-semibold "> Savoyardes
            <span class="fa fa-star border-1 border-light-green-atypik text-light-green-atypik"></span>
            <span class="fa fa-star  text-light-green-atypik"></span>
            <span class="fa fa-star  text-light-green-atypik"></span>
            <span class="fa fa-star  text-light-green-atypik"></span>
            <span class="fa fa-star  text-light-green-atypik"></span>
        </h3>
        <h4 class="mt-4 ml-3 text-xs font-bold text-dark-grey-atypik"> Hébergements</h4>
        <p class=" ml-4 mt-2 text-xs font-bold text-dark-grey-atypik "><i class=' mr-2 h-4 w-4   text-light-green-atypik fas'>&#xf186;</i> 13 nuits de 17/11/2022 au 30/11/2022</p>
        <p class=" ml-4  mt-2 text-xs font-bold text-dark-grey-atypik"><i class=" mr-2 h-4 w-4   text-light-green-atypik fa">&#xf236;</i> 5 personnes - 5 chambres</p>
        <div class=" mt-4 border-1 bg-dark-grey-atypik h-1 mb-6 ml-5 mr-5">
        </div>


        <h3 class=" ml-4 font-bold text-dark-grey-atypik">
            Pour un tarif total du :
        </h3>
        <p class=" mr-4 text-right font-extrabold text-light-green-atypik">806 <i class="text-base text-light-green-atypik fa">&#xf153;</i>
            HT</p>
        <p class="mr-4 mb-20 text-xs text-right font-bold text-gray-400">non flexible <br> non modifiable non remboursable</p>

        <button class="bg-light-green-atypik  w-11/12 ml-5 mb-5 rounded-full  text-light-grey1-atypik  p-2 "><a href="#/../Panier4">
                <h3 class="leading-3 text-lg font-semibold mb-0"> Continuer</h3>
                <h4 class=" text-xs">Sans Personnalisation</h4>
            </a></button>
        <h3 class=" ml-4 font-bold text-dark-grey-atypik">
            A propos de nos tarifs
        </h3>
        <p class="ml-4 mb-4 text-xs font-semibold text-dark-grey-atypik ">Les prix affichés comprennent l'hébergement ainsi que toutes les charges et taxes (excepté la taxe de séjour).
        </p>
        <p class=" ml-4 mb-6 text-xs font-semibold text-dark-grey-atypik ">Appelez nos conseillers au 070246100 (0,16<i class="text-sx  fa">&#xf153;</i> /min), du lundi au vendredi de 10h à 19h et le samedi 10h à 17h.</p>
    </div>

    <!-- </aside> -->

</section>

<script>
    flecheConciergerie = document.getElementById('fleche_conciergerie');
    var flecheRestauration = document.getElementById('fleche_restauration');

    flecheConciergerie.addEventListener('click', function() {
        this.classList.toggle('fa-rotate-90');
        let conciergerie = document.getElementById('conciergerie');
        conciergerie.classList.toggle('hidden');
    });

    flecheRestauration.addEventListener('click', function() {
        this.classList.toggle('fa-rotate-90');
        let restauration = document.getElementById('restauration');
        restauration.classList.toggle('hidden');
    });
</script>

@endsection
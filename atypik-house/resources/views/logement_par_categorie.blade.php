@extends('base.base')
@section('content')

<!-- section contenant tous les resultats -->
<section class="flex mb-6 p-4 justify-evenly">
    @foreach($logements as $logement)
        <div class=" w-1/5 rounded-md border border-green-atypik">
            <div class="w-full h-44 rounded-t-md overflow-hidden">
                <img src="{{$logement->image}}" alt="image du logement" class="min-h-full">
            </div>
            <h2>{{ $logement->nom }}</h2>
            <p>Capacité: {{$logement->capacite}} personnes</p>
            <p class="">superficie: {{$logement->superficie}} m²</p>
            <a href="/logements/{{$logement->id}}"><div class="bg-green-atypik mt-1 p-2 rounded-b-md text-center text-white">En savoir plus</div></a>
        </div>
    @endforeach
</section>

@endsection
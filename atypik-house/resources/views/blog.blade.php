@extends('base.base')
@section('content')

    <section class="articles">

        <div class="article">

            <div class="left">
                <img class="imag" src="https://cdn.france-montagnes.com/sites/default/files/bulle-insolite-combloux.jpg" alt="Bulle Romantique">
            </div>

            <div class="right p1">
                <span class="date">Mai, 21-2022</span>

                <h2 class="title">Bulle Romantique</h2>

                <P class="Description">Vous avez toujours rêvé de vivre dans votre bulle ? Atypik-House Immo (Haute-Savoie) propose de dormir sous une immense bulle transparente, face à la majestueuse chaîne du mont Blanc, en retrait à la lisière de la forêt. On s’endort en regardant les étoiles ou les flocons de neige, confortablement installés dans un lit douillet de 160 cm. Une nuit insolite... la tête dans les étoiles ! Vous pouvez même organiser un dîner romantique avec un chef rien que pour vous.</P>
                <span class="auteur">Coline</span>
            </div>
        </div>


        <div class="article">
            <div class="left">
                <img class="imag" src="https://i.pinimg.com/736x/ec/ca/31/ecca31f65846513914385e3cc2595c41--hobbit.jpg" alt="Maison Hobbit">
            </div>

            <div class="right p1">
                <p class="date">Mars, 01-2022</p>

                <h2 class="title">La Maison Des Hobbit </h2>

                <p class="Description">La maison de hobbit est un gîte écologique qui peut être une attraction touristique ou bien une petite maison privée. Croyez ou pas elles existent vraiment!</p>
                <span class="auteur">Anne Julie</span>
            </div>
        </div>


        <div class="article">

            <div class="left">
                <img class="imag" src="https://les-cles-de-la-maison.devis-plus.com/wp-content/uploads/2011/07/14426.jpg" alt="Chambre Piscine">
            </div>

            <div class="right p1">
                <p class="date"> Juin, 15-2022</p>

                <h2 class="title">Chambre Piscine</h2>

                <p class="Description">Rien de tel qu’un petit plongeon le matin pour se réveiller. Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt eveniet ipsum vel sed corporis soluta similique? Non eos, veritatis sit sed dolorum enim id consequatur perferendis excepturi, consectetur amet cupiditate.</p>
                <span class="auteur">Senia</span>
            </div>
        </div>


        <div class="article">

            <div class="left">
                <img class="imag" src="https://api.cloudly.space/resize/cropratioresize/460/345/60/aHR0cDovL21lZGlhcy50b3VyaXNtLXN5c3RlbS5jb20vZS81LzY4MjMwOV8xNzc1OTg0MTVfMTAxNjUzMTM5NDEzNTAxNDVfMTY2MTY0MDI2NjQyODM1NTg1M19uLmpwZw==/image.jpg" alt="Chambre Atypik Enfant ">
            </div>

            <div class="right p1">
                <p class="date">Avril, 21-2022</p>

                <h2 class="title">Chambre Atypic Enfant</h2>

                <p clas="Description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem sint, quibusdam excepturi libero est repellendus? Ipsam earum quod accusamus cum consectetur. Inventore sequi ipsa ad asperiores, doloribus ex dolorem explicabo.</p>
                <span class="auteur">Imen</span>
            </div>
        </div>

        <div class="article">

            <div class="left">
                <img class="imag" src="https://www.campings.com/img/_/landscape-medium/76851/d9ff8e5b-bf0e-4690-aeb1-7fc5e9c86928.jpg/_.jpg" alt="Goutte D'eau ">
            </div>

            <div class="right p1">
                <p class="date">Avril, 21-2022</p>

                <h2 class="title">Chambre Goutte D'eau</h2>

                <p clas="Description">La Goutte d'O, l'insolite dernière génération !
                Un cocon chaleureux pour un séjour en famille ou entre amoureux.
                Situé sur le canal de l'abbaye qui parcours le domaine verdoyant, les gouttes d'O sont la dernière nouveauté du Lieu Dieu !
                Disposées sur une plateforme surplombant l'eau, les baies vitrées de la goutte d'eau vous apporte une vue à 180 degrés pour une vue imprenable sur la beauté de la nature environnante : une vue étangs, rivière et prairie où gambadent les chevaux pour le plus grand plaisir des amoureux de la nature et des choses simples ! Un petit cocon de bonheur pour profiter d'un séjour en famille ou entre amoureux...</p>
                <span class="auteur">Adrien</span>
            </div>


    </section>
@endsection
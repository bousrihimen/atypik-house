    @extends('base.base')
    @section('content')
    <header class="-z-10 bg-center overflow-hidden h-96 opacity-45">
        <video autoplay muted loop id="myVideo"  class="w-full h-auto align-center" >
            <source src="{{ asset('/video/Visitez nos hébergements disponibles sur notre site  Atypik-houseimmo.com.mp4')}}" type="video/mp4">
        </video>
    </header>
        <div class="w-full bg-light-green-atypik p-3 text-center">
            <form action="/recherche" method="get">
                <label for="destination" class="text-light-grey1-atypik">Destination:</label>
                <input class="rounded-md p-1 text-dark-grey-atypik text-right" type="text" name="destination" placeholder="Lyon">
                <label for="nombre-vacanciers" class="text-light-grey1-atypik">Nombre de vacanciers:</label>
                <input class="rounded-md p-1 text-dark-grey-atypik text-right" type="number" name="nombre-vacanciers" placeholder="5">
                <label for="date-debut" class="text-light-grey1-atypik">Date d'arrivée:</label>
                <input class="rounded-md p-1 text-dark-grey-atypik text-right" type="date" name="date-debut">
                <label for="date-fin" class="text-light-grey1-atypik">Date de départ:</label>
                <input class="rounded-md p-1 text-dark-grey-atypik text-right" type="date" name="date-fin">
                <button type="submit" class="bg-dark-green-atypik hover:bg-light-green-atypik rounded-lg ml-6 py-2 px-3 text-light-grey1-atypik">Rechercher</button>
            </form>
        </div>
    
    <h1 class=" mt-6 text-light-grey3-atypik text-center text-3xl font-semibold">DECOUVREZ NOS HEBERGEMENTS INSOLITES</h1>
    
    <!-- barre horizontale verte -->
    <div class="h-1 bg-light-green-atypik mt-5 mb-5 w-1/4 mx-auto"></div>


    <!-- Carousel -->
    <div id="wrapper">
        <div id="carousel">
            <div id="content">

                <!-- images -->
                @foreach($categories as $categorie)
                    <div class="item relative w-[400px] h-[400px]  m-8 ">
                        <a href="/categories/{{$categorie->id}}">
                            <img class=" w-[400px] h-[400px]  rounded-xl object-cover" src="{{ $categorie->image }}">
                            <div class="absolute rounded-b-xl bottom-0 w-full  px-5 py-3 bg-black/40 text-center font-bold text-xl font-serif text-white">{{ $categorie->nom }}</div>
                        </a>
                    </div>
                @endforeach
                
            </div>
        </div>

        <!-- Boutons du Carousel -->
        <button id="prev">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path fill="none" d="M0 0h24v24H0V0z" />
                <path d="M15.61 7.41L14.2 6l-6 6 6 6 1.41-1.41L11.03 12l4.58-4.59z" />
            </svg>
        </button>
        <button id="next">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path fill="none" d="M0 0h24v24H0V0z" />
                <path d="M10.02 6L8.61 7.41 13.19 12l-4.58 4.59L10.02 18l6-6-6-6z" />
            </svg>
        </button>
    </div>

    <!-- Fin de Carousel -->

    <h2 class="mt-6 text-slate-400 text-center text-3xl font-semibold">LES AVIS DE NOS VOYAGEURS</h2>

    <!-- barre horizontale verte -->
    <div class="h-1 bg-light-green-atypik mt-5 w-1/4 mx-auto"></div>

    <section class="flex p-8 justify-evenly mt-4">
        <!-- Card avis n°1 -->
        <div class="w-1/5">
            <img class="rounded-full border border-gray-100 shadow-sm mx-auto" src="https://randomuser.me/api/portraits/men/21.jpg" alt="user image" />
            <h5 class="text-light-green-atypik text-center"><b>Lucien B.</b></h5>
            <div class="text-center">
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
            </div>
            
            <h5 class="text-dark-grey-atypik text-center"><b>Super site !</b></h5>

            <p class="text-center text-dark-grey-atypik">Une très belle découverte, dépaysement total pour ceux qui aiment la nature.</p>
        </div>
        <!-- Card avis n°2 -->
        <div class="w-1/5">
            <img class="rounded-full border border-gray-100 shadow-sm mx-auto" src="https://randomuser.me/api/portraits/women/82.jpg" alt="user image" />
            <h5 class="text-light-green-atypik text-center"><b>Lucie B.</b></h5>
            <div class="text-center">
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
            </div>
            
            <h5 class="text-dark-grey-atypik text-center"><b>Super site !</b></h5>

            <p class="text-center text-dark-grey-atypik">Une très belle découverte, dépaysement total pour ceux qui aiment la nature.</p>
        </div>
        <!-- Card avis n°3 -->
        <div class="w-1/5">
            <img class="rounded-full border border-gray-100 shadow-sm mx-auto" src="https://randomuser.me/api/portraits/men/63.jpg" alt="user image" />
            <h5 class="text-light-green-atypik text-center"><b>Albert H.</b></h5>
            <div class="text-center">
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
            </div>
            
            <h5 class="text-dark-grey-atypik text-center"><b>Super site !</b></h5>

            <p class="text-center text-dark-grey-atypik">Une très belle découverte, dépaysement total pour ceux qui aiment la nature.</p>
        </div>
        <!-- Card avis n°4 -->
        <div class="w-1/5">
            <img class="rounded-full border border-gray-100 shadow-sm mx-auto" src="https://randomuser.me/api/portraits/women/31.jpg" alt="user image" />
            <h5 class="text-light-green-atypik text-center"><b>Mathilde G.</b></h5>
            <div class="text-center">
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
                <i class="fa-solid fa-star text-light-green-atypik"></i>
            </div>
            
            <h5 class="text-dark-grey-atypik text-center"><b>Super site !</b></h5>

            <p class="text-center text-dark-grey-atypik">Une très belle découverte, dépaysement total pour ceux qui aiment la nature.</p>
        </div>
    </section>

    <!-- encart pour proposer un hebergement -->
    <div class="flex flex-col h-96 bg-devenir-hote bg-cover py-8 justify-center font-bold">
        <h2 class="text-center text-light-grey1-atypik text-4xl">Proposez votre hébergement atypique sur Atypik-houseimmo.com et augmentez vos revenus</h2>
        <div class="flex justify-center pt-12">
            <button class="shadow-lg bg-light-green-atypik hover:bg-dark-green-atypik text-light-grey1-atypik py-4 px-4 h-full rounded-full inline-flex items-center"><a href="#">Devenir Hôte</a></button>
        </div>
    </div>

    @endsection
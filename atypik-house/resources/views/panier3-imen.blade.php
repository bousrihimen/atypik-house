@extends('base.base')
@section('content')
    {{-- barre avancement de commande --}}
 <section class="flex mx-auto h-24 m-6 px-8 py-2 p-4 w-3/4 gap-2 ">   

      <div class="place-self-center flex flex-col w-40">      
         <i class="flex1 inline-block fa-solid fa-suitcase fa-2xl  text-[#008000]"></i> 
               <span class="flex justify-center text-xs text-[#008000]">Récapitulatif</span>
      </div>      
      <div class="inline-block self-center mt-5 w-1/4 mx-auto">      
         <div class="w-full bg-gray-400 h-1 mb-6">         
            <div class=" bg-[#008000] h-1" style="width: 100%">
            </div>       
         </div>     
      </div>  

     <div class="place-self-center flex flex-col w-40">       
        <i class="flex2  text-[#008000] inline-block fa-solid fa-bell-concierge fa-2xl"></i>
               <span class="flex justify-center text-xs  text-[#008000] ">Activités & services
                </span>     
     </div>      
     <div name="" class="inline-block self-center mt-5 w-1/4 mx-auto">       
        <div class="w-full bg-gray-400 h-1 mb-6">         
            <div class=" bg-[#008000] h-1" style="width: 100%">
            </div>       
        </div>     
    </div>  

   
    <div class="place-self-center flex flex-col w-40">
       <i class="flex3 inline-block fa-solid fa-address-card fa-2xl text-[#008000]"></i>
       <span class="flex justify-center text-xs text-[#008000]">Coordonnées</span>
    </div>

    <div name="" class="inline-block self-center mt-5 w-1/4 mx-auto">       
        <div class="w-full bg-gray-400 h-1 mb-6">         
            <div class=" bg-[#008000] h-1" style="width: 50%">
            </div>       
        </div>     
    </div> 

    <div class="place-self-center flex flex-col w-40">
      <i class="flex4 inline-block fa-solid fa-credit-card fa-2xl text-gray-400"></i>
      <span class="flex justify-center text-xs text-gray-400">Paiement</span>
    </div>

</section>      
{{-- section coordonner et valider commande panier3  --}}

{{-- coordonner --}}

<section class=" mt-6 mb-10 flex-row items-center justify-between  flex  p-12 ">

 <div class="text-lg font-medium  shadow-moi border-2 w-7/12  rounded-3xl shadow-gray-600   ">
    <div class=" ml-10 font-semibold text-green-800 text-3xl mt-16">
      <h2> Mes coordonnées</h2>
    </div>
    <div class=" ml-10 ">
        <label class="block text-gray-400 mt-4 text-lg font-semibold" for="">Nom</label>
        <input placeholder="Votre nom" class ="placeholder-gray-200 border-1 bg-gray-300  w-11/12 mb-3  p-2 rounded" type="text">
    </div>
    <div class="ml-10 ">
        <label class="block text-gray-400 text-lg font-semibold" for="">Prénom</label>
        <input placeholder="Votre prénom" class ="placeholder-gray-200 border-1 bg-gray-300  w-11/12 mb-3 p-2 rounded " type="text">
    </div>
    <div class="ml-10 ">
        <label class="block text-gray-400 text-lg font-semibold" for="">Email</label>
        <input placeholder="Votre adresse mail" class ="placeholder-gray-200 border-1 bg-gray-300  w-11/12 mb-3 p-2 roundedborder-2 " type="email">
    </div>
    <div class="ml-10 ">
        <label class="block text-gray-400 text-lg font-semibold" for="">Téléphone</label>
        <input placeholder="Votre numéro de téléphone" class ="placeholder-gray-200 border-1 bg-gray-300  w-11/12 mb-3 p-2 rounded " type="tel">
    </div>
    <div class="ml-10 ">
        <label class="block text-gray-400 text-lg font-semibold" for="">Adresse</label>
        <input placeholder="Votre adresse postale" class =" placeholder-gray-200 border-1 bg-gray-300  w-11/12 mb-3 p-2 rounded " type="text">
    </div>
    <div class="ml-10 ">
        <label class="block text-gray-400 text-lg font-semibold" for="">Code Postal</label>
        <input placeholder="Votre code postale"  class ="placeholder-gray-200 border-1 bg-gray-300  w-11/12 mb-3 p-2 rounded"  type="text">
    </div>
    <div class=" ml-10 ">
        <label class="block text-gray-400 text-lg font-semibold" for="">Ville</label>
        <input placeholder="Votre ville" class ="placeholder-gray-200 border-1 bg-gray-300  w-11/12 mb-3 p-2 rounded" type="text">
    </div>
    <div class="ml-10 ">
        <label class="block text-gray-400 text-lg font-semibold" for="">Pays</label>
        <input placeholder="Votre pays" class ="placeholder-gray-200 border-1 bg-gray-300  w-11/12 mb-3 p-2 rounded " type="text">
    </div>
    <div class="mb-6 mt-6 pr-14 flex items-end  justify-end ">
        <button class="bg-green-700 p-2 w-40 rounded-full text-white">Valider</button>
    </div>  
 </div>


 {{-- detaille et contenu --}}

 <div class="  w-4/12 ml-10 rounded-3xl shadow-gray-600 shadow-moi border-2">
    <div class=" w-auto">
        <img class =" rounded-t-3xl " src="{{ asset('img/chaletpanier3.jpg') }}"alt="">
    </div>
    <h3 class="  text-gray-400 text-2xl ml-8 mt-6 font-semibold"> Chalet des Grues Hautes </h3>
    <h3 class="  text-gray-400 text-2xl ml-8 mt-2 font-semibold "> Savoyardes   
        <span class="fa fa-star border-1 border-green-700 text-green-600"></span>
        <span class="fa fa-star  text-green-600"></span>
        <span class="fa fa-star  text-green-600"></span>
        <span class="fa fa-star  text-green-600"></span>
        <span class="fa fa-star  text-green-600"></span>
    </h3>
    <h4 class=" ml-3 text-xs font-bold text-gray-400"> Hébergements</h4>
    <p class=" ml-4 mt-2 text-xs font-bold text-gray-400 "><i  class=' mr-2 h-4 w-4   text-green-600 fas'>&#xf186;</i> 13 nuits de 17/11/2022 au 30/11/2022</p>
    <p class=" ml-4  mt-2 text-xs font-bold text-gray-400"><i  class=" mr-2 h-4 w-4   text-green-600 fa">&#xf236;</i>  5 personnes - 5 chambres</p>
    <div class=" mt-4 border-1 bg-gray-300 h-1 mb-6 ml-5 mr-5">              
     </div>  

     <h3 class=" ml-4 font-bold text-gray-400  ">  
        Pour un tarif total du : 
    </h3>
        <p class=" mr-4 text-right font-extrabold text-green-600"> 1200 <i class="text-base text-green-600 fa">&#xf153;</i>
            HT</p>
        <p class="mr-4 mb-20 text-xs text-right font-bold text-gray-400">non flexible <br> non modifiable non remboursable</p>
 
    <button class="bg-green-700  w-11/12 ml-5 mb-5 rounded-full  text-white  p-2 "><a href="#/../Panier4"><h3 class="leading-3 text-lg font-semibold mb-0"> Continuer</h3> <h4 class=" text-xs">Sans Personnalisation</h4></a></button>
    <h3 class=" ml-4 font-bold text-gray-400" >
        A propos de nos tarifs
     </h3>
     <p class="ml-4 mb-4 text-xs font-semibold text-gray-400 ">Les prix affichés comprennent l'hébergement ainsi que toutes les charge et taxes (excepté la taxe de séjour).
     </p>
     <p class=" ml-4 mb-6 text-xs font-semibold text-gray-400 ">Appelez nos conseillers au 070246100 (0,16<i class="text-sx  fa">&#xf153;</i> /min), du lundi au vendredi de 10h à 19h, le samedi 10h à 17h.</p>
</div>
</section>
@endsection
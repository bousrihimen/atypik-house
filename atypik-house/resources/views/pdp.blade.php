@extends('base.base')
@section('content')
<h3> QUI EST RESPONSABLE DU TRAITEMENT DE VOS DONNÉES À CARACTÈRE PERSONNEL ?</h3>

    <p>
        Le responsable du traitement de vos données est PUNTO FA, S.L. (« Atypik-House»), dont le numéro NIF est B-59088948. Les coordonnées de ATYPIC-HOUSE sont les suivantes :

        - Adresse postale : Atención al Cliente,

        - Numéro de téléphone : 09 67 89 75 35

        - E-mail : personaldata@atypik-house.com

        Nous vous informons que ATYPIK-HOUSE a désigné un délégué à la protection des données, à qui vous pourrez poser toute question relative au traitement de vos données à caractère personnel. Vous pourrez contacter le délégué à la protection des données aux coordonnées suivantes : dpo@atypik-house.com, adresse postale : .


    <h3>QUEL TYPE D’INFORMATIONS PERSONNELLES COLLECTE ATYPIK-HOUSE ?</h3>

    MANGO peut collecter les informations personnelles des utilisateurs énoncées ci-après par le biais de formulaires correspondants sur le site Internet de ATIPYK-HOUSE et/ou sur l’application mobile de ATIPYK-HOUSE (« SITE/APPLI ») : nom, courriel, adresse postale, numéro de téléphone, date de naissance, sexe et carte de crédit (PAN+CVV2) conformément à la norme PCI DSS ou numéro de compte (fourni par le client en cas de retour) lorsque l’utilisateur visite le SITE/l’APPLI, s’inscrit.

    De plus, ATYPIK-HOUSE collecte toutes les informations relatives à votre navigation sur le SITE/l’APPLI ainsi qu’à votre interaction avec la marque.

    Toutes les données à caractère personnel que collecte ATYPIK-HOUSE sont fournies par l’utilisateur, à l’exception des données de contact qui peuvent nous être fournies par des amis ou des membres de la famille pour l’envoi d’un chèque cadeau ou d’un autre type de cadeau.

    </p>
<<<<<<< HEAD
@endsection
=======
</body>

</html>
>>>>>>> origin/blog

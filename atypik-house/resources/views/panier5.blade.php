@extends('base.base')
@section('content')
    {{-- detaille et contenu --}}
    <section>
        <div class="">
            <h1 class="text-center  py-8 ml-4 mt-2 text-3xl font-bold text-gray-400 ">Confirmation de votre réservation</h1>
        </div>
        <div class="max-w-2xl mx-auto px-4 py-8 lg:max-w-7xl grid-cols-1 gap-y-10 gap-x-8 sm:grid-cols-2  lg:grid-cols-3 xl-grid-cols-4 w-3/4 m-6 rounded-2xl border-2 shadow-lg ">
 
            <div class="relative h-64 mx-0 overflow-hidden  rounded-lg" >
                <img src="{{ asset('img/cabane1.jpg') }}"alt="">
            </div class="ml-6 mt-6">
            <h3 class="  text-gray-400 text-2xl ml-6 mt-6 font-semibold"> Chalet des Grues Hautes Savoyardes

                <span class="fa fa-star border-1 border-green-700 text-green-600"></span>
                <span class="fa fa-star  text-green-600"></span>
                <span class="fa fa-star  text-green-600"></span>
                <span class="fa fa-star  text-green-600"></span>
                <span class="fa fa-star  text-green-600"></span>
            </h3>

            <h3 class="text-gray-400 text-2xl  ml-6 mt-5 mb-5 font-semibold"> Hébergements</h3>
            <p class=" ml-4 mt-2 text-xs font-bold text-gray-400 "><i
                    class=' mr-2 h-4 w-4   text-green-600 fas'>&#xf186;</i> 13 nuits de 17/11/2022 au 30/11/2022</p>
            <p class=" ml-4  mt-2 text-xs font-bold text-gray-400"><i class=" mr-2 h-4 w-4   text-green-600 fa">&#xf236;</i>
                5 personnes - 5 chambres</p>
            <div class=" mt-4 border-1 bg-gray-300 h-1 mb-6 ml-5 mr-5">
            </div>

            <h3 class=" ml-4 font-bold text-gray-400  ">
                Pour un tarif total de :
            </h3>
            <p class=" mr-4 text-right font-extrabold text-green-600"> 1200 <i
                    class="text-base text-green-600 fa">&#xf153;</i> HT
            </p>
            <p class="mr-4 mb-20 text-xs text-right font-bold text-gray-400">non flexible <br> non modifiable non
                remboursable</p>



            <p class=" ml-4 mb-6 text-xs font-semibold text-gray-400 ">Appelez nos conseillers au 070246100 (0,16<i
                    class="text-sx  fa">&#xf153;</i> /min), du lundi au vendredi de 10h à 19h, le samedi 10h à 17h.</p>
        </div>
        <div>
            <h2 class="text-center  py-8 ml-4 mt-2 text-xl font-bold text-gray-400 ">Nous vous remercions de votre
                réservation et
                nous vous souhaitons un très bon séjours!</h2>
        </div>
    </section>
@endsection

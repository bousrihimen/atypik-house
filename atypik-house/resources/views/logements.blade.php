@extends('base.base')
@section('content')

    <!-- barre de recherche -->
<div class=" bg-[#dddbdb] p-4 mt-4 mx-4 rounded-lg">
    <form action="/recherche" method="get" class="flex flex-col">
        <div class="flex">
            <div class="w-1/2 p-2">
                <label for="destination" class=" text-gray-800 text-xl">Destination:</label>
                <input class="w-full rounded-md p-1 bg-gray-400 text-white placeholder:text-slate-200 placeholder:italic" type="text" name="destination" placeholder="ville, code postal, nom de domaine ...">
            </div>
            <div class="w-1/2 p-2">
                <label for="nombre-vacanciers" class=" text-gray-800 text-xl">Nombre de vacanciers:</label>
                <input class="w-full rounded-md p-1 bg-gray-400 text-white placeholder:text-slate-200 placeholder:italic" type="number" name="nombre-vacanciers" placeholder="nombre de vacanciers">
            </div>
        </div>
        <div class="flex">
            <div class="w-1/2 p-2">
                <label for="date-debut" class=" text-gray-800 text-xl">Date d'arrivée:</label>
                <input class="w-full rounded-md p-1 bg-gray-400 text-white placeholder:text-slate-100" type="date" name="date-debut">
            </div>
            <div class="w-1/2 p-2">
                <label for="date-fin" class=" text-gray-800 text-xl">Date de départ:</label>
                <input class="w-full rounded-md p-1 bg-gray-400 text-white placeholder:text-slate-100" type="date" name="date-fin">
            </div>
        </div>
        <button type="submit" class="bg-green-atypik hover:bg-green-700 text-white mt-2 px-7 py-1 rounded-3xl mx-auto">Rechercher</button>
    </form>
</div>

<h1 class=" mt-6 text-slate-400 text-center text-3xl font-semibold">Trouvez le logement Atypik de vos rêves</h1>
    
    <!-- barre horizontale verte -->
    <div class="h-1 bg-green-atypik my-5 w-1/4 mx-auto"></div>

<!-- section contenant tous les logements -->
<section class="flex mb-6 p-4 justify-evenly">
    @foreach($logements as $logement)
        <div class=" w-1/5 rounded-md border border-green-atypik">
            <div class="w-full h-44 rounded-t-md overflow-hidden">
                <img src="{{$logement->image}}" alt="image du logement" class="min-h-full">
            </div>
            <h2>{{ $logement->nom }}</h2>
            <p>Capacité: {{$logement->capacite}} personnes</p>
            <p class="">superficie: {{$logement->superficie}} m²</p>
            <a href="/logements/{{$logement->id}}"><div class="bg-green-atypik mt-1 p-2 rounded-b-md text-center text-white">En savoir plus</div></a>
        </div>
    @endforeach
</section>

@endsection
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
    <title>Footer</title>
</head>
<body>
<footer>

    <div class="debut">
        <img src="https://trello.com/1/cards/62aaf0b8898cbb088ac3d9d1/attachments/62b042281a81af4ffaed3d4c/previews/62b042281a81af4ffaed3d56/download/image.png" width="120" height="110" alt="logo"></img>
    
        <div class="contenu-footer">
            <div class="bloc-footer-liste">
                <ul>
                    <li><a href="">Accueil</a></li>
                    <li><a href="">Hébergement</a></li>
                    <li><a href="/blog">Blog</a></li>
                </ul>
            </div>
            <div class="bloc-footer-liste">
                <ul>
                    <li><a href="/nouscontacter">Contact</a></li>
                    <li><a href="">Qui sommes-nous</a></li>
                    <li><a href="/faq">FAQ</a></li>
                </ul>
            </div>
            <div class="bloc-footer-liste">
                <ul>
                    <li><a href="">Recrutement</a></li>
                    <li><a href="">Presse</a></li>
                    <li><a href="/mentions-legales">Mentions Légales</a></li>
                </ul>
            </div>
        </div>
    </div>
            <div class="footer-newsletter">
                <h4>Abonnez-vous à notre newsletter et recevez les offres spéciales et promotions sur les locations de vacances.</h4>
            </div>
            <section>
                <form method="post" action="mailto:votreemail@email.com">
                    <input class="mail" type="email" placeholder="Votre adresse e-mail">
	                <input class="button" type="submit" value="S'abonner" />
                </form>
            </section>
        <hr>
            <div class="fin">
                <div class="phrasecopyright">
                    <div class="CGU-PDP"><i class="fa-regular fa-copyright"></i>
                        2022 Atypik-House, tous droits réservés. <a href="">Conditions Générales d'Utilisation</a>.
                    </div>
                    <div class="CGU-PDP p">
                        <a href=""> Politiques des Données Personnelles</a>
                    </div>
                </div>

                <div class="logos">
                    <a href="https://fr-fr.facebook.com"><i class="fa-brands fa-facebook"></i></a>
                    <a href="https://www.instagram.com"><i class="fa-brands fa-instagram"></i></a>
                    <a href="https://fr.linkedin.com"><i class="fa-brands fa-linkedin"></i></a>
                    <a href="http://www.twitter.fr/"><i class="fa-brands fa-twitter"></i></a>
                    <a href="https://www.tiktok.com/fr"><i class="fa-brands fa-tiktok"></i></a>
                </div>
            </div>
                

</footer>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html> 

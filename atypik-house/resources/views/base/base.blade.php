<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
    <link rel="stylesheet" href="{{ asset('css/index1.css') }}">
    <link rel="stylesheet" href="{{ asset('css/indexlamia.css') }}">
    <title>Atypik house</title>
</head>
<body>
<header id="header" class=" shadow-lg">
      
      <div class=" flex overflow-hidden    h-24 grid-cols-3 gap-4  justify-around">
           <a  href="/"><img class=" hover:shadow-lg h-full w-auto my-2" src="{{ asset('img/logo.png') }}" ></a>
     
               <div class=" flex flex-nowrap   grid-cols-3 gap-4 items-center">
                     
                          <a class="nav-link text-lg font-semibold text-dark-grey-atypik hover:underline decoration-4  decoration-light-green-atypik hover:font-bold" href="/">Accueil</a>

                          <a class="nav-link text-lg font-semibold text-dark-grey-atypik hover:underline decoration-4  decoration-light-green-atypik hover:font-bold" href="/logements">Hébergement</a>
                      
                          <a class="nav-link text-lg font-semibold text-dark-grey-atypik hover:underline decoration-4  decoration-light-green-atypik hover:font-bold" href="/blog">Blog</a>

                          <a class="nav-link text-lg font-semibold text-dark-grey-atypik hover:underline decoration-4  decoration-light-green-atypik hover:font-bold" href="/nouscontacter"> Contact</a>

                          <a class="nav-link text-lg font-semibold text-dark-grey-atypik hover:underline decoration-4  decoration-light-green-atypik hover:font-bold" href="/devenir-hote">Devenir Hôte</a>

                          <a class="nav-link text-lg font-semibold text-dark-grey-atypik hover:underline decoration-4  decoration-light-green-atypik hover:font-bold hover:text-light-grey3-atypik" href="/faq">FAQ</a>
                     
                  <div  class="flex flex-nowrap grid-cols-3 gap-4 content-center h-12">
                      <div class="content-center">
                          <a href="/panier"><button class=" shadow-lg  bg-light-green-atypik hover:bg-dark-green-atypik text-light-grey1-atypik font-bold py-1 h-full px-3 rounded-lg ">
                              <svg   class="fill-current w-1 h-1 mr-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 2"> </svg>
                              <i style="font-size:25px" class="fa ">&#xf07a;</i>
                          </button></a>
                      </div>
                      <div class="">
                        <button class=" shadow-lg bg-light-green-atypik hover:bg-dark-green-atypik text-light-grey1-atypik py-2 px-2 h-full rounded-full inline-flex items-center" onclick="showModal()">
                          <svg class="fill-current w-1 h-1 " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 2"> </svg>
                          <div><span class="mr-2 text-xl">
                          Se Connecter </span><i style="font-size:40px" class=" fa align-middle">&#xf2bd;</i></div>
                        </button>
                      </div>
                  </div>
              </div>
       </div>

    

        <div class="z-10 bg-black bg-opacity-50 absolute inset-0 hidden justify-center items-center" id="overlay">
            <div class="z-20 bg-light-grey1-atypik rounded-lg">
        
                <div class="flex justify-between items-center mr-4">
                    <h4 class="text-xl font-bold text-dark-grey-atypik"> Se Connecter </h4>
                    <div onclick="hideModal()"><i class="fa-solid fa-x text-dark-grey-atypik cursor-pointer"></i></div>
                </div>
                <div class="flex h-[21em] w-[19em] mx-auto">
                    <aside class=" h-full w-full mt-4 mx-4">
                        <!-- <div class="text-gray-400 w-full">
                             <div class="flex space-x-2">
                                <button class="inline-block w-full h-8 bg-[#008000] text-white rounded-md text-center">Google</button>
                                <button class="inline-block w-full h-8 bg-[#008000] text-white rounded-md text-center">Facebook</button>
                            </div>
                        </div> -->

                        <form action="/auth/login" method="post" class="z-50 flex flex-col">
                        @csrf
                            <div class="flex flex-col h-2/3 w-full mt-6">
                                <label for="email" class="text-dark-grey-atypik">Email</label>
                                <span class="relative">
                                    <input type="email" name="email" class="mb-2 border-2 border-gray-400 rounded-sm w-full" required>
                                </span>
                                <label for="mdp" class="text-dark-grey-atypik">Mot de passe</label>
                                <span class="relative">
                                    <input id="mdpConnexion" type="password" name="password" class="w-full mb-2 border-2 border-gray-400 rounded-sm" minlength="8" max-lenght="20" required>
                                    <i id="eyeMdpConnexion" onclick="showOrHideMdp('mdpConnexion')" class="fa-solid fa-eye absolute top-[0.4rem] right-2"></i>
                                </span>
                            </div>
                            <div class="flex justify-evenly text-xs font-medium mt-6 space-x-5">
                                <span>
                                    <input type="checkbox" name="seSouvenirDeMoi">
                                    <label for="SeSouvenirDeMoi" class="text-dark-grey-atypik">Se souvenir de moi</label>
                                </span>
                                <span class="text-dark-grey-atypik underline">
                                    <a href="#">Mot de passe oublié ?</a>
                                </span>
                            </div>
                            <div class="flex justify-between items-center mt-6 space-x-2">
                                <button type="submit" class="bg-white text-light-green-atypik border-solid border-light-green-atypik border-2 hover:bg-dark-green-atypik hover:text-light-grey3-atypik rounded-3xl text-sm inline-block w-1/2 p-2">Se Connecter</button>
                                <button type="button" class="bg-light-green-atypik border-2 text-white rounded-3xl text-sm inline-block w-1/2 p-2"><a href="/auth">Créer un compte</a></button>
                            </div>
                        </form>
                    </aside>
                </div>
            </div>    
        </div>
   
</header>

@yield('content')

<footer class="absolute flex h-auto w-full bg-dark-grey-atypik">
    <div class="relative mx-auto text-light-grey1-atypik">
        <div class="flex items-stretch mx-[250px] my-[20px] text-xl tracking-wide">
            <div class="mr-6 w-[135px] h-[140px]">
                <img class="rounded" src="{{ asset('img/logo.png') }}" alt="logo">
            </div>
            <div class="flex justify-between space-x-36">
                <div class="flex flex-col">
                    <ul>
                        <li><a href="#">Accueil</a></li>
                        <li><a href="#">Hébergement</a></li>
                        <li><a href="#">Blog</a></li>
                    </ul>
                </div>
                <div class="flex flex-col">
                    <ul>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Qui sommes-nous</a></li>
                        <li><a href="#">FAQ</a></li>
                    </ul>
                </div>
                <div class="flex flex-col">
                    <ul>
                        <li><a href="#">Recrutement</a></li>
                        <li><a href="#">Presse</a></li>
                        <li><a href="#">Mentions Légales</a></li>
                    </ul>
                </div>
            </div>    
        </div>

        <div class="flex flex-col mx-[250px] my-[5px] space-y-4">
            <div class="text-2xl text-light-grey1-atypik font-semibold">Abonnez-vous à notre newsletter et recevez les offres spéciales et promotions sur les locations de vacances.</div>       
                <form class="flex justify-between w-full" method="post" action="mailto:votreemail@email.com">
                    <input class="p-3 w-3/5 text-lg rounded" type="email" placeholder="Votre adresse e-mail">
                    <div class="inline-block">
                        <button class="bg-light-green-atypik hover:bg-dark-green-atypik text-light-grey1-atypik p-3 w-[350px] rounded-2xl" onclick="showModal()">
                          <svg class="fill-current w-1 h-1 " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 2"></svg>
                            <a href="#"><span class="text-lg">S'abonner</span></a>
                        </button>
                    </div>
                </form>
            
        </div>

        <div class="flex justify-between mx-[250px] my-12 border-t-light-grey1-atypik border-t-2">
            <div class="text-base">
                <div class="flex items-center space-x-2 mt-2"><i class="fa-regular fa-copyright"></i>
                    2022 Atypik-House, tous droits réservés. <div class="underline text-sm"><a href="/conditions-generales"> Conditions Générales d'Utilisation - </a></div>
                </div>
                <div class="flex justify-start text-sm underline">
                    <a href="/politique-donnees-personnelles"> Politiques des Données Personnelles </a>
                </div>
            </div>

            <div class="flex items-center space-x-4 mt-2">
                <a href="https://fr-fr.facebook.com"><i class="fa-brands fa-facebook text-light-grey1-atypik fa-2xl"></i></a>
                <a href="https://www.instagram.com"><i class="fa-brands fa-instagram text-light-grey1-atypik fa-2xl"></i></a>
                <a href="https://fr.linkedin.com"><i class="fa-brands fa-linkedin text-light-grey1-atypik fa-2xl"></i></a>
                <a href="http://www.twitter.fr/"><i class="fa-brands fa-twitter text-light-grey1-atypik fa-2xl"></i></a>
                <a href="https://www.tiktok.com/fr"><i class="fa-brands fa-tiktok text-light-grey1-atypik fa-2xl"></i></a>
            </div>
        </div>
    </div>      

</footer>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/panier.js') }}"></script>
    <script src="{{ asset('js/inscription_connexion.js') }}"></script>
    <script src="{{ asset('js/carousel.js') }}"></script>
</body>
</html>
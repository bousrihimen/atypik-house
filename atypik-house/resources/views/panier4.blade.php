@extends('base.base')
@section('content')
<div class="">

    {{-- barre avancement de commande  --}}
    <div class="flex mx-auto   m-12 px-8 py-2 w-3/4 gap-2">   

        <div class="place-self-center flex flex-col w-40">      
            <i class="flex1 inline-block fa-solid fa-suitcase fa-2xl  text-[#008000]"></i> 
                <span class="flex justify-center text-xs text-[#008000]">Récapitulatif</span>
        </div> 

        <div class="inline-block self-center mt-5 w-1/4 mx-auto">      
             <div class="w-full bg-gray-400 h-1 mb-6">         
                <div class=" bg-[#008000] h-1" style="width: 100%">
                </div>       
            </div>     
        </div>  

        <div class="place-self-center flex flex-col w-40">       
            <i class="flex2  text-[#008000] inline-block fa-solid fa-bell-concierge fa-2xl"></i>
                <span class="flex justify-center text-xs text-[#008000] ">Activités & services
                    </span>     
        </div>   

        <div class="inline-block self-center mt-5 w-1/4 mx-auto">       
            <div class="w-full bg-gray-400 h-1 mb-6">         
                <div class=" bg-[#008000] h-1" style="width: 100%">
                </div>       
            </div>     
        </div>  

   
        <div class="place-self-center flex flex-col w-40">
            <i class="flex3 inline-block fa-solid fa-address-card fa-2xl text-[#008000]"></i>
            <span class="flex justify-center text-xs text-[#008000]">Coordonnées</span>
        </div>

        <div class="inline-block self-center mt-5 w-1/4 mx-auto">       
            <div class="w-full bg-gray-400 h-1 mb-6">         
                <div class=" bg-[#008000] h-1" style="width: 100%">
                </div>       
            </div>     
        </div> 

        <div class="place-self-center flex flex-col w-40">
             <i class="flex4 inline-block fa-solid fa-credit-card fa-2xl text-green-800"></i>
             <span class="flex justify-center text-xs text-[#008000]">Paiement</span>
        </div>
    </div>

     
  {{-- div coordonner et valider commande panier3 --}}

  {{-- coordonner --}}

    <div name="" class=" mt-10 mb-10  inline-flex   ">

        <div class="ml-8 p-4 text-lg font-medium  shadow-moi border-2 w-7/12  rounded-3xl shadow-gray-600   ">
            <div class=" ml-10 font-semibold text-green-800 text-3xl mt-16">
                <h2> Mes coordonnées</h2>
            </div>
                <ul class=" ml-10 list-none mx-2 text-gray-400  font-bold leading-none">
                    <li> Jean </li>
                    <li>Paul</li>
                    <li>JeanPaul@orange.fr</li>
                    <li>+33 0689369616</li>
                    <li>2 avenue Des Saules</li>
                    <li> 59160, France</li>
                </ul>
                    {{-- ligne de separtion gris --}}
                <div  name =""class=" mt-8 border-1 bg-gray-300 h-1 mb-6 ml-5 mr-5">              
                </div>  
                <div class=" ml-10 font-semibold text-green-800 text-3xl mt-12">
                    <h2> Moyen de paiement</h2>
                </div>
                    <h4  class=" mb-3 font-bold text-base text-gray-400">Code promo</h4>
                <input  class ="ml-10 placeholder-gray-200 border-1 w-7/12 mb-3 p-2 rounded bg-gray-300 " placeholder="Ajouter  un code promo" type="text">
                <button class=" text-white border-1 w-2/12 mb-3 p-2 rounded bg-green-700 " placeholder="Ajouter  un code promo"><a href="">Ok</a></button>
                {{-- moyen de payement --}}
                <div name="" class="text-gray-600 ml-10 inline-flex"> 
                    <div class=" rounded-xl mr-10  text-white bg-gray-600 hover:bg-green-600 font-bold py-1 px-5"><a href="#"> <i class=" active:text-green-700 text-10xl fa-brands fa-cc-visa"></i></a>
                    </div>
                    <div class=" rounded-xl text-white bg-gray-600 hover:bg-green-600 font-bold py-1 px-5"><a href="#"> <i class=" active:text-green-700 text-10xl  fa-brands fa-cc-mastercard"></i></a>
                    </div>
                </div>
                <div class="text-gray-600  ml-10 inline-flex mt-5">
                    <div class=" rounded-xl mr-10  text-white bg-gray-600 hover:bg-green-600 font-bold py-1 px-5"><a href="#"> <i class=" active:text-green-700 text-10xl  fa-brands fa-cc-amex"></i></a>
                    </div>
                    <div class=" rounded-xl  text-white bg-gray-600 hover:bg-green-600 font-bold py-1 px-5"><a href="#"> <i class=" active:text-green-700 text-10xl fa-brands fa-cc-paypal"></i></a>
                    </div>
                </div>

                <div class=" ml-10 ">
                    <label class="block text-gray-400 mt-4 text-lg font-semibold break-normal" for="">Titulaire</label>
                    <input placeholder="Nom du titulaire de la carte" class ="placeholder-gray-200 border-1 bg-gray-300  w-11/12 mb-3  p-2 rounded" type="text">
                </div>
 
                <div class="inline-flex">
                    <div class="ml-10 float-left ">
                        <label class=" text-gray-400 mt-4 text-lg font-semibold break-normal" for="">Numéro de carte</label><br>
                        <input placeholder=" xxxx xxxx xxxx xxxx " class =" w-96 placeholder-gray-200 border-1 bg-gray-300  p-2 rounded" type="text">
                    </div>
                    <div class="ml-1 float-right">
                        <label class=" ml-1  text-gray-400 mt-4 text-lg font-semibold" for="">Crypto</label><br>
                        <input placeholder="xxx" class =" w-56 ml-2 placeholder-gray-200 border-1  bg-gray-300   p-2 rounded" type="text">
                    </div>
                </div>

                <div class=" mr-10 ml-10 pr-10 flex justify-between">
                    <span>
                        <input type="checkbox" name="souvenir" class=" p-4 border-gray-400">
                        <label for="souvenir" class="text-gray-500 text-sm font-medium ">J'ai bien pris connaissance et j'accepte  les conditions générales et particulieres de vente, ainsi que les  documents d'information et les conditions générales et particulières de vente des assurances. <br>
                        <a href="/conditions-generales" class="text-gray-500 underline dark:md:hover:bg-fuchsia-600">Voir les CGV </a> et <a  href="/politique-donnees-personnelles" class=" underline
                             hover:underline  "> les documents d'informations des assurances.</a>
                        </label>
                    </span>
                </div>

                <div class="mb-6 mt-6 pr-14 flex items-end  justify-end ">
                    <button class="bg-green-700 p-2 w-11/12 rounded-full text-white">Confirmer & Payer</button>
                </div>  
        </div>

      {{-- detaille et contenu --}}

     <div class="  mb-96  w-4/12 ml-10 rounded-3xl  border-2">
        <div class=" w-auto">
            <img class =" rounded-t-3xl " src="{{ asset('img/panier4.jpg') }}"alt="">
        </div>
        <h3 class="  text-gray-400 text-2xl ml-8 mt-6 font-semibold"> Chalet des Grues Hautes </h3>
        <h3 class="  text-gray-400 text-2xl ml-8 mt-2 font-semibold "> Savoyardes   
            <span class="fa fa-star border-1 border-green-700 text-green-600"></span>
            <span class="fa fa-star  text-green-600"></span>
            <span class="fa fa-star  text-green-600"></span>
            <span class="fa fa-star  text-green-600"></span>
            <span class="fa fa-star  text-green-600"></span>
        </h3>
        <h4 class=" ml-3 text-xs font-bold text-gray-400"> Hébergements</h4>
            <p class=" ml-4 mt-2 text-xs font-bold text-gray-400 "><i  class=' mr-2 h-4 w-4   text-green-600 fas'>&#xf186;</i> 13 nuits de 17/11/2022 au 30/11/2022</p>
            <p class=" ml-4  mt-2 text-xs font-bold text-gray-400"><i  class=" mr-2 h-4 w-4   text-green-600 fa">&#xf236;</i>  5 personnes - 5 chambres</p>
        <div class=" mt-4 border-1 bg-gray-300 h-1 mb-6 ml-5 mr-5">              
        </div>  

        <h3 class=" ml-4 font-bold text-gray-400  ">  
        Pour un tarif total du : 
        </h3>
            <p class=" mr-4 text-right font-extrabold text-green-600"> 1200 <i class="text-base text-green-600 fa">&#xf153;</i>
            HT</p>
            <p class="mr-4 mb-12 text-xs text-right font-bold text-gray-400">non flexible <br> non modifiable non remboursable</p>
 
   
        <h3 class=" ml-4 font-bold text-gray-400" >
            A propos de nos tarifs
        </h3>
            <p class="ml-4 mb-4 text-xs font-semibold text-gray-400 ">Les prix affichés comprennent l'hébergement ainsi que toutes les charge et taxes (excepté la taxe de séjour).
            </p>
            <p class=" ml-4 mb-6 text-xs font-semibold text-gray-400 ">Appelez nos conseillers au 070246100 (0,16<i class="text-sx  fa">&#xf153;</i> /min), du lundi au vendredi de 10h à 19h, le samedi 10h à 17h.</p>
       </div>
    </div>
</div>

@endsection
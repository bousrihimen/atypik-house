@extends ('base.base')

@section ('content')


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Panier</title>
</head>

<body>

  <section class="flex mx-auto h-24 w-3/4">

    <div class="place-self-center flex flex-col w-40">
      <i class="flex1 inline-block fa-solid fa-suitcase fa-2xl text-dark-green-atypik"></i>
      <span class="flex justify-center text-xs text-dark-green-atypik">Récapitulatif</span>
    </div>

    <div class="inline-block self-center mt-5 w-1/4 mx-auto">
      <div class="w-full bg-light-grey3-atypik h-1 mb-6">
        <div class=" bg-dark-green-atypik h-1" style="width: 50%"></div>
      </div>
    </div>

    <div class="place-self-center flex flex-col w-40">
      <i class="flex2 inline-block fa-solid fa-bell-concierge fa-2xl text-light-grey3-atypik"></i>
      <span class="flex justify-center text-xs text-light-grey3-atypik">Activités & services</span>
    </div>

    <div class="inline-block self-center mt-5 w-1/4 mx-auto">
      <div class="w-full bg-light-grey3-atypik h-1 mb-6">
        <div class=" bg-light-grey3-atypik  h-1" style="width: 50%"></div>
      </div>
    </div>

    <div class="place-self-center flex flex-col w-40">
      <i class="flex3 inline-block fa-solid fa-address-card fa-2xl text-light-grey3-atypik"></i>
      <span class="flex justify-center text-xs text-light-grey3-atypik">Coordonnées</span>
    </div>

    <div class="inline-block self-center mt-5 w-1/4 mx-auto">
      <div class="w-full bg-light-grey3-atypik h-1 mb-6">
        <div class=" bg-light-grey3-atypik h-1" style="width: 50%"></div>
      </div>
    </div>

    <div class="place-self-center flex flex-col w-40">
      <i class="flex4 inline-block fa-solid fa-credit-card fa-2xl text-light-grey3-atypik"></i>
      <span class="flex justify-center text-xs text-light-grey3-atypik">Paiement</span>
    </div>

  </section>

  <section class="flex flex-col mx-auto h-screen w-3/4 rounded-lg border-solid border-non border-2 shadow-xl p-8 bg-gray-50">
    
    <div class="flex w-fit">
      
      <div class="mx-10 my-2"><img src="../img/chalet.jpg" alt="chalet"></div>

      <div class="mt-2">
        
        <div>
          <h3 class="inline-block text-gray-600 text-lg font-bold">Chalet Des Grues Hautes Savoyardes</h3>
          <i class="ml-6 place-self-center fa-solid fa-star text-[#008000]"></i>
          <i class="place-self-center fa-solid fa-star text-[#008000]"></i>
          <i class="place-self-center fa-solid fa-star text-[#008000]"></i>
          <i class="place-self-center fa-solid fa-star text-[#008000]"></i>
          <i class="place-self-center fa-solid fa-star-half-stroke text-[#008000]"></i>
        </div>

        <div class="flex leading-[4]">
          
          <div class="text-gray-500">
            <h6>Votre date de réservation</h6>

            <div date-rangepicker="" class="flow-root">
              <div class="relative">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path>
                  </svg>
                </div><input name="start" type="text" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-sm block w-full pl-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white datepicker-input" placeholder="Date d'arrivée" required>
              </div>

              <div class="relative">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                  <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path>
                  </svg>
                </div><input name="end" type="text" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-sm block w-full pl-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white datepicker-input" placeholder="Date de départ" required>
              </div>
            </div>
          
            <div>
              <h6>Nombre de vacanciers</h6>

              <div>
                <label for="vacanciers" class="block text-sm font-light text-gray-500"></label>
                <input type="number" min="0" id="vacanciers" class="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-sm block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white" placeholder="" required>
              </div>
            </div>
          </div>

          <div class="mt-20">
            <div class="flex justify-start mb-2">
              <i class="fa-solid fa-bed text-[#008000] mx-2"></i>
              <p class="text-gray-500 text-xs">5 chambres</p>
            </div>
            <div class="flex justify-start mb-2">
              <i class="fa-solid fa-square text-[#008000] mx-2"></i>
              <p class="text-gray-500 text-xs">143 m²</p>
            </div>
            <div class="flex justify-start">
              <i class="fa-solid fa-house-user text-[#008000] mx-2"></i>
              <p class="text-gray-500 text-xs">Idéal pour 8 personnes</p>
            </div>
          </div>
        </div>
        <div>
          <div class="flex text-gray-500 mt-12 justify-between">
            <div class="text-[#008000] italic underline text-sm">
            <a href="#">Plus de détails +</a>
            </div>

            <div class="self-center">
              <button class="bg-green-600 hover:bg-green-600 text-white font-bold py-2 px-10 rounded-3xl"><span>Mettre à jour</span></button>
            </div>
          </div>
        </div>
      </div>
    </div>
   
    <div class="flex w-fit flex-col px-2 py-2 border-t-gray-300 border-t-2 ml-10 mt-6">

      <div class="flex justify-between text-gray-500">

        <div class="self-center">
          <strong>Non flexible :</strong> non modifiable, non remboursable<i class="fa-solid fa-circle-info ml-4 text-sm text-[#008000]"></i>
        </div>
        
        <div>
          <table class="table-auto">
            <thead>
              <tr class="font-thin">
                <th class="border-r-gray-300 border-r-2 px-4">Nuitées</th>
                <th class="border-r-gray-300 border-r-2 px-4">Prix</th>
                <th class="px-4">Total</th>
              </tr>
            </thead>
            <tbody>
              <tr class="text-[#008000] text-sm">
                <td class="border-r-gray-300 border-r-2 px-4">12 nuitées</td>
                <td class="border-r-gray-300 border-r-2 px-4">62 € HT / nuit</td>
                <td class="px-4">744 € HT</td>
              </tr>
            </tbody>
          </table>
        </div>
        
        <div class="self-center">
          <button class="bg-green-600 hover:bg-green-600 text-white font-bold py-2  px-16 rounded-3xl"><span>Choisir</span></button>
        </div>

      </div>

      <div class="flex flex-col px-1 py-2 border-t-gray-300 border-t-2 mt-6">
      
      <div class="flex justify-between text-gray-500">

        <div class="self-center flex">
          <strong>Flexible : </strong>
          <div>&nbspannulable et modifiable jusqu'au <i class="fa-solid fa-circle-info ml-4 text-sm text-[#008000]"></i><br><span> 14/11/2022</span> </div>
        </div>
        
        <div>
          <table class="table-auto ml-10">
            <thead>
              <tr class="font-thin">
                <th class="border-r-gray-300 border-r-2 px-4">Nuitées</th>
                <th class="border-r-gray-300 border-r-2 px-4">Prix</th>
                <th class="px-4">Total</th>
              </tr>
            </thead>
            <tbody>
              <tr class="text-[#008000] text-sm">
                <td class="border-r-gray-300 border-r-2 px-4">12 nuitées</td>
                <td class="border-r-gray-300 border-r-2 px-4">70 € HT / nuit</td>
                <td class="px-4">840 € HT</td>
              </tr>
            </tbody>
          </table>
        </div>
        
        <div class="self-center">
          <button class="bg-green-600 hover:bg-green-600 text-white font-bold py-2  px-16 rounded-3xl"><span>Choisir</span></button>
        </div>

      </div>
    </div>

  </section>
  
  <section class="h-screen">

  </section>

  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

@endsection
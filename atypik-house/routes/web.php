<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//______ACCUEIL_______
Route::get('/', App\Http\Controllers\HomeController::class);

//________AUTH________
Route::get('/isauth', [App\Http\Controllers\CustomAuthController::class,'test']); // Route de test
Route::get('/auth', [App\Http\Controllers\CustomAuthController::class,'index']);
Route::post('/auth/inscription',[App\Http\Controllers\CustomAuthController::class,'customRegistration']);
Route::post('/auth/login',[App\Http\Controllers\CustomAuthController::class,'customLogin']);
Route::get('/auth/logout', [App\Http\Controllers\CustomAuthController::class,'signOut']);

// ROUTES DES LOGEMENTS ET CATEGORIES
Route::get('/logements', [App\Http\Controllers\LogementController::class,'index']); 
Route::get('/logements/{id}', [App\Http\Controllers\LogementController::class,'show']); 
Route::get('/categories',[App\Http\Controllers\Categorie_logementController::class,'index']);
Route::get('/categories/{id}',[App\Http\Controllers\Categorie_logementController::class,'show']);
Route::get('/recherche', [App\Http\Controllers\LogementController::class,'search']);

//ROUTES POUR DEVENIR HOTE
Route::get('/devenir-hote', function () {
    return view('devenir_hote');
});

Route::get('/devenir-hote-valide', function () {
    return view('devenir_hote_confirmation');
});

Route::get('/add', function () {
    return view('ajout_hebergement');
}); // vue ajout hebergement non trouvée



// ROUTES DE CONTACT / FAQ / CONTACT / MENTION LEGALES
Route::get('/contact', function (){

    return view('contact');
});

Route::get('/faq', function (){

    return view('faq');
});
Route::get('/nouscontacter', function (){

    return view('nousContacter');
});
Route::get('/mentions-legales', function (){

    return view('mentionsLegales');
});
Route::get('/conditions-generales', function (){

    return view('cgvu');
});
Route::get('/politique-donnees-personnelles', function(){return view('pdp');});

Route::get('/blog', function (){

    return view('blog');
});
Route::get('/panier2', function (){

    return view('panier2');
});


Route::get('/panier', function(){
    return view('panier');
});

Route::get('/panier3', function(){
    return view('panier3-imen');
});   
Route::get('/panier4', function(){
    return view('panier4');
});

Route::get('/panier5', function (){

    return view('panier5');
});

<?php
 
namespace App\Http\Controllers;
 
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
 
class CustomAuthController extends Controller
{
 
    public function index()
    {
        return view('auth.inscription_connexion');
    }  
       
 
    public function customLogin(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
    
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
 
            return redirect()->intended('/');
        }
 
        return back()->withErrors([
            'login' => 'email ou mot de passe érroné',
        ])->onlyInput('email');
    }
       
 
    public function customRegistration(Request $request)
    {  
        $request->validate([
            'prenom' => 'bail|required|min:2|max:255',
            'nom' => 'bail|required|min:2|max:255',
            'email' => 'bail|required|email|unique:App\Models\User,email',
            'password' => 'required|size:8|max:255',
            'confirm-mdp' => 'required|same:password'
        ]);
            
        $data = $request->all();

        $this->create($data);
          
        return redirect("/");//->withSuccess('have signed-in')
    }
 
 
    public function create(array $data)
    {
      return User::create([
        'prenom' => $data['prenom'],
        'nom' => $data['nom'],
        'email' => $data['email'],
        'password' => Hash::make($data['password'])
      ]);
    }     
 
    public function signOut() {
        Session::flush();
        Auth::logout();
   
        return Redirect('/auth');
    }

    public function test()
    {
        dd(Auth::check());
    }
}


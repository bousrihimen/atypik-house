<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Logement;
use App\Http\Controllers\HomeController;

class LogementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logements = Logement::all();
        return view('logements',["logements"=> $logements]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Url::create($request->all());        
        return redirect('/'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $logement = Logement::findOrFail($id);
        return view('detail_logement', [
            'logement' => $logement
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Logement $logement)
    {
        return view('edit',['logement' => $logement]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $u = Url::find($request->id);
        $u->update($request->all());
        return redirect('/show/'.$request->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Logement $logement)
    {
        $logement->delete();
        return redirect ("/");
    }

    /**
     * Search function of the Homepage
     * 
     */
    public function search()
    {
        $destination = request()->input('destination');
        $nombreVacanciers = request()->input('nombre-vacanciers');
        $dateDebut = request()->input('date-debut');
        $dateFin = request()->input('date-fin');

        if(!$destination) return redirect('/');

        $logements = Logement::where('capacite','>=', "$nombreVacanciers")
                              ->where('adresse', 'like', "%$destination%")
                              ->orWhere('particularite', 'like', "%$destination%")
                              ->get();
        
        
        return view('resultat_recherche', [
            'logements' => $logements,
            'destination' => $destination,
            'nombreVacanciers' => $nombreVacanciers,
            'dateDebut' => $dateDebut,
            'dateFin' => $dateFin
        ]);
        
    }
}

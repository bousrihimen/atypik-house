<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categorie_logement;
class HomeController extends Controller
{
    public function __invoke()
    {
        $categories = Categorie_logement::all();
        return view('accueil',[
            'categories' => $categories
        ]);
    }
}

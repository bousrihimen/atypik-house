<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Logement;
use App\Models\Categorie_logement;

class Categorie_logementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categorie_logement::all();
        return view('categories', [
            'categories' => $categories
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $logements = Logement::where('categorie_id', $id)->get();
        return view('logement_par_categorie',[
            'logements' => $logements
        ]);
    }

}

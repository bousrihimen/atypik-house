/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './resources/views/**/*.blade.php',
    './resources/views/*.blade.php',
  ],
  theme: {
    extend: {
      fontSize:{
       '10xl':'160px'
      },
      width:{
        '100':'500px'
     },
 
       
    
    
      backgroundImage: {
        'header-atypik': "url('/img/header-atypik.jpg')",
        'devenir-hote' : "url('/img/devenir-hote.png')",
        'cabane' : "url('/images/cabane.jpg')",
        'cabane1' : "url('/images/cabane.jpg')",
        'yourte' : "url('/images/yourte.jpg)"
      },
      colors: {
        'green-atypik' : '#008000',
        'green-dark-atypik' : '#34907c',
        
        'light-green-atypik' : '#0B9D53',
        'dark-green-atypik' : '#076837',
        'red-atypik' : '#ED3A3A',
        'dark-grey-atypik' : '#6C757D',
        'light-grey3-atypik' : '#D9D9D9',
        'light-grey2-atypik' : '#E2E2E2',
        'light-grey1-atypik' : '#FAF8F8',
      },
      spacing: {
        '5px': '5px',
      }
    },
  },
  plugins: [],

}